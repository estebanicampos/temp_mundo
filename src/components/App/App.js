import React, { useState } from 'react';

//css
import './App.css';
import './Header.css';
import '../Search/Search';

//components
import Search from '../Search/Search';
import Days from '../Days/Days';

function App() {
  const [weather, setWeather] = useState({
    name: '',
    sys: {
      country: '',
    },
  });

  console.log(weather);

  return (
    <div className='app-container background-img bg-cold'>
      <Search setWeather={setWeather} />
      <div className='header-city'>
        {weather.name},{weather.sys.country?.toLowerCase()}
      </div>
      <Days weather={weather} />
    </div>
  );
}

export default App;
