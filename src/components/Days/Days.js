import React from 'react';

import './styles.css';

const Days = ({ weather }) => {
  const dateBuilder = (d) => {
    let months = [
      'Enero',
      'Febreo',
      'Marzo',
      'Abril',
      'Mayo',
      'Junio',
      'Julio',
      'Agosto',
      'Septimbre',
      'Octubre',
      'Noviembre',
      'Diciembre',
    ];
    let days = [
      'Domingo',
      'Lunes',
      'Martes',
      'Miercoles',
      'Jueves',
      'Viernes',
      'Sabado',
    ];

    let day = days[d.getDay()];
    let date = d.getDate();
    let month = months[d.getMonth()];
    let year = d.getFullYear();

    return `${day} ${date} ${month} ${year}`;
  };

  return (
    <div className='days-box  owl-carousel owl-theme'>
      {typeof weather.main != 'undefined' ? (
        <div>
          <div className='day item'>
            <div className='date'> {dateBuilder(new Date())}</div>
            <div className='temp'>{Math.round(weather.main.temp)}°c</div>
            <div className='main'>{weather.weather[0].main}</div>
          </div>
        </div>
      ) : (
        ''
      )}
    </div>
  );
};

export default Days;
