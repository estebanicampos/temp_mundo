import React, { useState, useEffect } from 'react';
import axios from 'axios';

import api from '../../config/config';

//css
import './Search.css';

const Search = ({ setWeather }) => {
  const [query, setQuery] = useState('Buenos Aires');
  const [queryGancho, setQueryGancho] = useState('Buenos Aires');

  useEffect(() => {
    const getWeather = async () => {
      const response = await axios({
        method: 'get',
        url: `${api.url}weather?q=${query}&cnt=5&units=metric&APPID=${api.key}`,
      });

      setWeather(response.data);
    };

    getWeather();
  }, [queryGancho]);

  const search = (e) => {
    console.log(e.key);

    if (e.key === 'Enter') {
      setQuery(e.currentTarget.value);
      setQueryGancho(e.currentTarget.value);
    }
  };

  return (
    <input
      type='text'
      className='search-input-city'
      placeholder='City'
      onChange={(e) => setQuery(e.target.value)}
      value={query}
      onKeyPress={search}
    />
  );
};

export default Search;
