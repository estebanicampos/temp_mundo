import axios from 'axios';

import api from '../../config/config';

const getWeather = () => {
  const response = axios({
    method: 'get',
    url: `${api.url}forecast?q=${query}&units=metric&APPID=${api.key}`,
  });
};

export default getWeather;
